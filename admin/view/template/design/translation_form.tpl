<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-translation" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
<!--      <h1>--><?php //echo $; ?><!--{{ heading_title }}</h1>-->
      <ul class="breadcrumb">
          <ul class="breadcrumb">
              <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
              <?php } ?>
          </ul>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
      <?php if ($error_warning) { ?>
          <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
      <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $text_form; ?> </h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-translation" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-store">entry_store</label>
            <div class="col-sm-10">
              <select name="store_id" id="input-store" class="form-control">
                <option value="0">text_default</option>
                  <?php foreach ($stores as $store): ?>
                  <?php if ($store['store_id'] == $store_id): ?>
                <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['name']; ?></option>
                  <?php else: ?>
                <option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
                  <?php endif; ?>
                  <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-language">entry_language</label>
            <div class="col-sm-10">
              <select name="language_id" id="input-language" class="form-control">
                  <?php foreach ($languages as $language): ?>
                  <?php if ($language['language_id'] == $language_id): ?>
                <option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                  <?php else: ?>
                <option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
                  <?php endif; ?>
                  <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-route">entry_route</label>
            <div class="col-sm-10">
              <select name="route" id="input-route" class="form-control">
                  <?php if ($route): ?>
                <option value="<?php echo $route; ?>" selected="selected"><?php echo $route; ?></option>
                  <?php endif; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-key">entry_key</label>
            <div class="col-sm-10">
              <select name="key" id="input-key" class="form-control">
                  <?php if ($key): ?>
                <option value="<?php echo $key; ?>" selected="selected"><?php echo $key; ?></option>
                  <?php endif; ?>
              </select>
              <input type="text" name="key" value="<?php echo $key; ?>" placeholder="entry_key" class="form-control" />
                <?php if ($error_key): ?>
              <div class="text-danger"><?php echo $error_key; ?></div>
                <?php endif; ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-default">entry_default</label>
            <div class="col-sm-10">
              <textarea name="default" placeholder="entry_default" rows="5" id="input-default" class="form-control" disabled="disabled">
                  default</textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-value">entry_value</label>
            <div class="col-sm-10">
              <textarea name="value" rows="5" placeholder="entry_value" id="input-value" class="form-control"><?php echo $value; ?></textarea>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
      <?php if ($key): ?>
    $('select[name="store_id"]').prop('disabled', true);
    $('select[name="language_id"]').prop('disabled', true);
    $('select[name="route"]').prop('disabled', true);
    $('select[name="key"]').prop('disabled', true);
    $('input[name="key"]').prop('disabled', true);
    {% else %}
    $('select[name="language_id"]').on('change', function() {
      $.ajax({
        url: 'index.php?route=design/translation/path&token=<?=$token?>&language_id=' + $('select[name="language_id"]').val(),
        dataType: 'json',
        beforeSend: function() {
          $('select[name="route"]').html('');
          $('select[name="key"]').html('');
          $('input[name="key"]').val('');
          $('textarea[name="default"]').val('');
          $('select[name="store_id"]').prop('disabled', true);
          $('select[name="language_id"]').prop('disabled', true);
          $('select[name="route"]').prop('disabled', true);
          $('select[name="key"]').prop('disabled', true);
          $('input[name="key"]').prop('disabled', true);
          $('textarea[name="value"]').prop('disabled', true);
        },
        complete: function() {
          $('select[name="store_id"]').prop('disabled', false);
          $('select[name="language_id"]').prop('disabled', false);
          $('select[name="route"]').prop('disabled', false);
        },
        success: function(json) {
          html = '<option value=""></option>';

          if (json) {
            for (i = 0; i < json.length; i++) {
              if (i == 0) {
                html += '<option value="' + json[i] + '" selected="selected">' + json[i] + '</option>';
              } else {
                html += '<option value="' + json[i] + '">' + json[i] + '</option>';
              }
            }
          }

          $('select[name="route"]').html(html);

          $('select[name="route"]').trigger('change');
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    });

    var translation = [];

    $('select[name="route"]').on('change', function() {
      $.ajax({
        url: 'index.php?route=design/translation/translation&token=<?=$token?>&language_id=' + $('select[name="language_id"]').val() + '&path=' + $('select[name="route"]').val(),
        dataType: 'json',
        <?php if (!$key): ?>
        beforeSend: function() {
          $('select[name="key"]').html('');
          $('input[name="key"]').val('');
          $('textarea[name="default"]').val('');
          $('textarea[name="value"]').val('');
          $('select[name="store_id"]').prop('disabled', true);
          $('select[name="language_id"]').prop('disabled', true);
          $('select[name="route"]').prop('disabled', true);
          $('select[name="key"]').prop('disabled', true);
          $('textarea[name="value"]').prop('disabled', true);
        },
        complete: function() {
          $('select[name="store_id"]').prop('disabled', false);
          $('select[name="language_id"]').prop('disabled', false);
          $('select[name="route"]').prop('disabled', false);
          $('select[name="key"]').prop('disabled', false);
          $('textarea[name="value"]').prop('disabled', false);
        },
        <?php endif; ?>
        success: function(json) {
          translation = [];

          html = '<option value=""></option>';

          if (json) {
            for (i = 0; i < json.length; i++) {
              translation[json[i]['key']] = json[i]['value'];

              if (i == 0) {
                html += '<option value="' + json[i]['key'] + '" selected="selected">' + json[i]['key'] + '</option>';
              } else {
                html += '<option value="' + json[i]['key'] + '">' + json[i]['key'] + '</option>';
              }
            }
          }

          $('select[name="key"]').html(html);

          $('select[name="key"]').trigger('change');
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    });

    $('select[name="language_id"]').trigger('change');

    $('select[name="key"]').on('change', function() {
      if ($(this).val()) {
        $('textarea[name="default"]').val(translation[$('select[name="key"]').val()]);
        $('input[name="key"]').val($('select[name="key"]').val());

        $('input[name="key"]').prop('disabled', true);
      } else {
        $('textarea[name="default"]').val('');

        $('input[name="key"]').prop('disabled', false);
      }
    });
      <?php endif; ?>
  //--></script> 
</div>
<?=$footer?>

