<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?=$add?>" data-toggle="tooltip" title="<?=$button_add?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?=$button_delete?>" class="btn btn-danger" onclick="confirm('<?=$text_confirm?>') ? $('#form-translation').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid"><?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> </h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-translation">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left">
                      <?php if ($sort == 'store'): ?>
                          <a href="<?php echo $sort_store; ?>" class="<?php echo $order; ?> <?php echo $lower; ?>">column_store</a>
                      <?php else: ?>
                          <a href="<?php echo $sort_store; ?>">column_store</a>
                      <?php endif; ?>
                  </td>
                  <td class="text-left">
                      <?php if ($sort == 'language'): ?>
                          <a href="<?php echo $sort_language; ?>" class="<?php echo $order; ?> <?php echo $lower; ?>">column_language</a>
                      <?php else: ?>
                          <a href="<?php echo $sort_language; ?>">column_language</a>
                      <?php endif; ?>
                  </td>
                  <td class="text-left">
                      <?php if ($sort == 'route'): ?>
                          <a href="<?php echo $sort_route; ?>" class="<?php echo $order; ?> <?php echo $lower; ?>">column_route</a>
                      <?php else: ?>
                          <a href="<?php echo $sort_route; ?>">column_route</a>
                      <?php endif; ?>
                  </td>
                  <td class="text-left">
                      <?php if ($sort == 'key'): ?>
                          <a href="<?php echo $sort_key; ?>" class="<?php echo $order; ?> <?php echo $lower; ?>">column_key</a>
                      <?php else: ?>
                          <a href="<?php echo $sort_key; ?>">column_key</a>
                      <?php endif; ?>
                  </td>
                  <td class="text-left">
                      <?php if ($sort == 'value'): ?>
                          <a href="<?php echo $sort_value; ?>" class="<?php echo $order; ?> <?php echo $lower; ?>">column_value</a>
                      <?php else: ?>
                          <a href="<?php echo $sort_value; ?>">column_value</a>
                      <?php endif; ?>
                  </td>
                  <td class="text-right">column_action
                  </td>
                </tr>
              </thead>
              <tbody>
              <?php if ($translations): ?>
              <?php foreach ($translations as $translation): ?>
              <tr>
                <td class="text-center"> <?php if (in_array($translation['translation_id'], $selected)): ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $translation['translation_id']; ?>" checked="checked" />
                    <?php else: ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $translation['translation_id']; ?>" />
                    <?php endif; ?></td>
                <td class="text-left"><?php echo $translation['store']; ?></td>
                <td class="text-left"><?php echo $translation['language']; ?></td>
                <td class="text-left"><?php echo $translation['route']; ?></td>
                <td class="text-left"><?php echo $translation['key']; ?></td>
                <td class="text-left"><?php echo $translation['value']; ?></td>
                <td class="text-right"><a href="<?php echo $translation['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
              </tr>
              <?php endforeach; ?>
              <?php else: ?>
              <tr>
                <td class="text-center" colspan="7">text_no_results</td>
              </tr>
              <?php endif; ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
