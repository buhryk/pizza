<?php 
class ModelModuleSeoUrlAutoVier extends Model{
    
    public function genKeyword() {
        if(isset($this->request->post['keyword']) && !$this->request->post['keyword']) {
            $text = '';
            $suff_url = false;
            $lang_id = (int)$this->config->get('config_language_id');
            if(isset($this->request->post['product_description'][$lang_id]['name'])) {
                $text = $this->request->post['product_description'][$lang_id]['name'];
                //$suff_url = true;
            }
            elseif(isset($this->request->post['information_description'][$lang_id]['title'])) {
                $text = $this->request->post['information_description'][$lang_id]['title'];
                //$suff_url = true;
            }
            elseif(isset($this->request->post['name'])) {
                $text = $this->request->post['name'];
                //$suff_url = true;
            }
            elseif(isset($this->request->post['manufacturer_description'][$lang_id]['name'])) {
                $text = $this->request->post['manufacturer_description'][$lang_id]['name'];
                //$suff_url = true;
            }
            elseif(isset($this->request->post['category_description'][$lang_id]['name'])) {
                $text = $this->request->post['category_description'][$lang_id]['name'];
            }
            $this->request->post['keyword'] = $this->translitEng($text, $suff_url);
        }
    }
    
    private function translitEng($russ_str, $suff_url = false) {
        if(!is_string($russ_str)) return '';
        if(strlen($russ_str) == 0) return '';
        $russ_str = mb_strtolower(trim(strip_tags(html_entity_decode($russ_str, ENT_QUOTES, 'UTF-8'))));
		$arr_replace = array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'э' => 'e', 'ё' => 'e', 'е' => 'e', 'ж' => 'j', 'з' => 'z', 'ы' => 'i', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'щ' => 'sch', 'ш' => 'sh', 'ч' => 'ch', 'я' => 'ya', 'ю' => 'yu', 'ъ' => '', 'ь' =>'','і' =>'i','ї' =>'i','є' => 'e'
        ,'ä' => 'a','ö' => 'o','ü' => 'u','û' => 'u','è' => 'e','é' => 'e','ß' => 's'
        ,'Ø' => 'd' ,'∅' => '','ø' => 'd','¹' => '1','²' => '2','³' => '3'
        ,'°' => '','—' => '-','´' => ''
        ,'~' => '-'
        ,'&quot'=>'','&nbsp;' => '',' ' => '-','_' => '-','"' => '',"'" => '','`' => '','>' => '','<' => '','=' => '-','!' => '-','@' => '-','#' => '','$' => '','%' => '','&' => '-','?' => '','(' => '',')' => '','[' => '',']' => '','{' => '','}' => '','+' => '-plus-',';' => '-','.' => '-',',' => '-','№' => '',':' => '','^' => '','/' => '-','|' => '-','\\' => '-','*' => '-');
		$translit = strtr($russ_str, $arr_replace);
        $translit = preg_replace('~[^-a-z0-9_]+~u', '', $translit);
        $translit = trim(preg_replace('/-+/u', '-', $translit),'-');
        $translit = substr($translit, 0, 150);
		if($suff_url && !$this->config->get('config_seo_url_type') && (strlen($translit) > 0)) $translit .=  '.html';
		return $translit;
	}
}
?>