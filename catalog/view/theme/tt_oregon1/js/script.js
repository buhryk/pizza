$(document).ready(function () {
    if ($('main').hasClass('page-home')){
        $('.page-home .slider-main .container').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            autoplay:true,
            autoplaySpeed: 3000,
            arrows: false,
        });
    }

    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 209){
            $('header').addClass('fixed');
        }
        else {
            $('header').removeClass('fixed');
        }
    });

    $('.mob-menu-btn').click(function () {
        $(this).toggleClass('open');
        $('.mob-menu').toggleClass('open');
    });
    $('.number .plus').click(function () {


        var thiss = $(this).closest('.number').find('.input');

            var num = $(this).closest('.number').find('.input').val();


                num++;

            $(this).closest('.number').find('.input').val(num);


        cardNum(thiss);
        basketPrice(thiss, num);
    });
    $('.number .minus').click(function () {

        // alert('e');
        var thiss = $(this).closest('.number').find('.input');

        if($(thiss).closest('.number').find('.d').hasClass('d')){
            var num = $(this).closest('.number').find('.input').val();
            if (num == 1 || num < 1){
                num = 0;
            }
            else {
                num--;
            }
            $(this).closest('.number').find('.input').val(num);
        }
        else{
            var num = $(this).closest('.number').find('.input').val();
            if (num < 2){
                num = 1;
            }
            else {
                num--;
            }
            $(this).closest('.number').find('.input').val(num);
        }
        cardNum(thiss);
        basketPrice(thiss, num);
    });


    function cardNum(thiss){

        console.log($(thiss).html());
        if(!$(thiss).closest('.number').find('.d').hasClass('d')){
            var th = thiss;
            var num = th.val();
            console.log(num);
            var product_price = parseInt(th.closest('.number').attr('data-price'));


            if(th.closest('.products-list').find('.prod-card').hasClass('prod-card')){
                var option_price = parseInt(th.closest('.opacity').find('input:checked').data('price'));
            }else {
                var option_price = parseInt(th.closest('.left-coll').find('.active .option').data('priceoption'));
            }
            option_price = option_price ? option_price : 0;

            var price = option_price + product_price;

            console.log(price);
            var priceNew = num * price;
            console.log(priceNew);
            var priceText = priceNew + ' Br';
            console.log(priceText);

            th.closest('.prod-card').find('.bottom .price').text(priceText);

            th.closest('.product-card').find('.bottom .price').text(priceText);
        }
    }
    function basketPrice(thiss, num){
        var th = thiss;
        var num = num;
        var k = parseFloat(th.closest('.product-row').find('.summ').attr('data-price'));
        var t = parseFloat(th.closest('.product-row').find('.summ').attr('data-total'));
        var sum = (num * k) + t;
        sum = sum + ' Br';
        th.closest('.product-row').find('.summ').find('p').text(sum);
    }
    $('.number .input').change(function () {
        var thiss = $(this);
        cardNum(thiss);
    });
    $("#phone").mask("+375 (99) 999-99-99");

    var height = $('.page-payment .content-tabs .box:nth-child(1)').height();
    $('.page-payment .content-tabs').height(height);

    $('.page-payment .filter li a').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.page-payment .filter li').removeClass('active');
        $(this).closest('li').addClass('active');
        var selector = '.page-payment .content-tabs .box:nth-child('+id+')';
        var height = $(selector).height();
        $('.page-payment .content-tabs .box').removeClass('open');
        $(selector).addClass('open');
        $('.page-payment .content-tabs .box').hide();
        $(selector).fadeIn();
        $('.page-payment .content-tabs').height(height);
    });
    $('.page-single .product-card .left-coll .size .row.act').click(function () {
        $('.page-single .product-card .left-coll .size .row.act').removeClass('active');
        $(this).addClass('active');
    });

    $('.dobavki .d-header p').click(function () {
        $(this).closest('.d-header').find('p').removeClass('active');
        $(this).addClass('active');
        var id = $(this).index();
        id = id*1 + 1;
        var selector = '.tab:nth-child('+id+')';
        $(this).closest('.dobavki').find('.d-body').find('.tab').removeClass('active');
        $(this).closest('.dobavki').find('.d-body').find(selector).addClass('active');
    });
});