<?php echo $header; ?>

<?php if($information_id == 4): ?>
      <main class="page-about">
            <section class="block-content">
                  <h2 class="section-title"><?=$heading_title?></h2>
                  <div class="content">
                        <div class="container">
                              <img src="<?=$image?>" alt="">
                            <?php echo $description; ?>
                        </div>
                  </div>
            </section>
      </main>
<?php elseif ($information_id == 5): ?>
    <?php
    $telephones = explode(',', $telephone);
    $socials = explode(',', $social);
    ?>
      <main class="page-contacts">
            <section class="title-block">
                  <div class="container">
                        <h2 class="section-title"><?=$heading_title?></h2>
                  </div>
            </section>
            <section class="table-contacts">
                  <div class="container">
                        <div class="top row desc">
                              <div class="box"><?=$language->get('contact_us')?></div>
                              <div class="box"><?=$language->get('address')?></div>
                              <div class="box"><?=$language->get('we_are_in_social_networks')?></div>
                        </div>
                        <div class="bottom row desc">
                              <div class="box">
                                    <p><?=$language->get('phone')?></p>
                                    <a href="tel:<?= str_replace(' ', '', $telephones[0]) ?>"><?= $telephones[0] ?></a>
                                    <p>E-mail</p>
                                    <a href="mailto:<?=$email?>"><?=$email?></a>
                              </div>
                              <div class="box">
                                    <p><?=$language->get('cafe')?> «<?=$name?>»</p>
                                    <p><?=$address?></p>
                                    <p><?=$opentime?></p>
                              </div>
                              <div class="box">
                                    <a target="_blank" href="<?=$socials[0]?>">
                                          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="vk" class="svg-inline--fa fa-vk fa-w-18" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M545 117.7c3.7-12.5 0-21.7-17.8-21.7h-58.9c-15 0-21.9 7.9-25.6 16.7 0 0-30 73.1-72.4 120.5-13.7 13.7-20 18.1-27.5 18.1-3.7 0-9.4-4.4-9.4-16.9V117.7c0-15-4.2-21.7-16.6-21.7h-92.6c-9.4 0-15 7-15 13.5 0 14.2 21.2 17.5 23.4 57.5v86.8c0 19-3.4 22.5-10.9 22.5-20 0-68.6-73.4-97.4-157.4-5.8-16.3-11.5-22.9-26.6-22.9H38.8c-16.8 0-20.2 7.9-20.2 16.7 0 15.6 20 93.1 93.1 195.5C160.4 378.1 229 416 291.4 416c37.5 0 42.1-8.4 42.1-22.9 0-66.8-3.4-73.1 15.4-73.1 8.7 0 23.7 4.4 58.7 38.1 40 40 46.6 57.9 69 57.9h58.9c16.8 0 25.3-8.4 20.4-25-11.2-34.9-86.9-106.7-90.3-111.5-8.7-11.2-6.2-16.2 0-26.2.1-.1 72-101.3 79.4-135.6z"/></svg>
                                    </a>
                                    <a target="_blank" href="<?=$socials[1]?>">
                                          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>
                                    </a>
                              </div>
                        </div>
                        <div class="mob-cont-co">
                              <div class="box title"><?=$language->get('contact_us')?></div>
                              <div class="box phon">
                                    <p><?=$language->get('phones')?></p>
                                    <a href="tel:<?= str_replace(' ', '', $telephones[0]) ?>"><?= $telephones[0] ?></a>
                                    <a href="tel:<?= str_replace(' ', '', $telephones[1]) ?>"><?= $telephones[1] ?></a>
                                    <p>E-mail</p>
                                    <a href="mailto:info@eda-mog.by"><?=$email?></a>
                              </div>
                              <div class="box title"><?=$language->get('address')?></div>
                              <div class="box addr">
                                    <p><?= $language->get('cafe') ?> «<?=$name?>»</p>
                                    <p><?=$address?></p>
                                    <p><?=$opentime?></p>
                              </div>
                              <div class="box title"><?=$language->get('we_are_in_social_networks')?></div>
                              <div class="box socials">
                                    <a target="_blank" href="<?=$socials[0]?>">
                                          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="vk" class="svg-inline--fa fa-vk fa-w-18" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M545 117.7c3.7-12.5 0-21.7-17.8-21.7h-58.9c-15 0-21.9 7.9-25.6 16.7 0 0-30 73.1-72.4 120.5-13.7 13.7-20 18.1-27.5 18.1-3.7 0-9.4-4.4-9.4-16.9V117.7c0-15-4.2-21.7-16.6-21.7h-92.6c-9.4 0-15 7-15 13.5 0 14.2 21.2 17.5 23.4 57.5v86.8c0 19-3.4 22.5-10.9 22.5-20 0-68.6-73.4-97.4-157.4-5.8-16.3-11.5-22.9-26.6-22.9H38.8c-16.8 0-20.2 7.9-20.2 16.7 0 15.6 20 93.1 93.1 195.5C160.4 378.1 229 416 291.4 416c37.5 0 42.1-8.4 42.1-22.9 0-66.8-3.4-73.1 15.4-73.1 8.7 0 23.7 4.4 58.7 38.1 40 40 46.6 57.9 69 57.9h58.9c16.8 0 25.3-8.4 20.4-25-11.2-34.9-86.9-106.7-90.3-111.5-8.7-11.2-6.2-16.2 0-26.2.1-.1 72-101.3 79.4-135.6z"/></svg>
                                    </a>
                                    <a target="_blank" href="<?=$socials[1]?>">
                                          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg>
                                    </a>
                              </div>
                        </div>
                  </div>
            </section>
            <section class="map">
                  <div class="container">
                      <?=$description?>
                  </div>
            </section>
      </main>
<?php elseif ($information_id == 6): ?>
    <?php
    $telephones = explode(',', $telephone);
    $socials = explode(',', $social);
    ?>
      <main class="page-payment">
            <section class="title-block">
                  <div class="container">
                        <h2 class="section-title"><?=$heading_title?></h2>
                  </div>
            </section>
            <section class="filter">
                  <div class="container">
                        <ul>
                              <li class="active"><a data-id="1" href="##"><?=$form_of_payment['title']?></a></li>
                              <li><a data-id="2" href="##"><?=$food_delivery['title']?></a></li>
                              <li><a data-id="3" href="##"><?=$food_delivery_to_office['title']?></a></li>
                              <li><a data-id="4" href="##"><?=$takeaway['title']?></a></li>
                        </ul>
                  </div>
            </section>
            <section class="content-tabs">
                  <div class="container">
                        <div class="box open">
                              <h3><?=$form_of_payment['title']?></h3>
                              <div class="content">
                                    <?=html_entity_decode($form_of_payment['description'], ENT_QUOTES, 'UTF-8')?>
                              </div>
                        </div>
                        <div class="box">
                              <h3><?=$food_delivery['title']?></h3>
                              <div class="content">
                                  <?=html_entity_decode($food_delivery['description'], ENT_QUOTES, 'UTF-8')?>
                              </div>
                        </div>
                        <div class="box">
                              <h3><?=$food_delivery_to_office['title']?></h3>
                              <div class="content">
                                  <?=html_entity_decode($food_delivery_to_office['description'], ENT_QUOTES, 'UTF-8')?>
                              </div>
                        </div>
                        <div class="box">
                              <h3><?=$takeaway['title']?></h3>
                              <div class="content">
                                  <?=html_entity_decode($takeaway['description'], ENT_QUOTES, 'UTF-8')?>
                              </div>
                        </div>
                  </div>
            </section>
      </main>
<?php elseif ($information_id == 11): ?>
      <main class="page-review">
            <section class="title-block">
                  <div class="container">
                        <h2 class="section-title"><?=$heading_title?></h2>
                        <div class="description">
                            <?=$description?>
                        </div>
                  </div>
            </section>
            <section class="form-comment">
                  <div class="container">
                        <h3><?=$language->get('leave_a_comment')?></h3>
                        <p class="descr">*<?=$language->get('required_fields')?></p>
                        <form action="" method="post" id="send-normal">
                              <div class="box">
                                    <span class="label"><?=$language->get('first_name_last_name')?>*</span>
                                    <input type="text" name="author" required>
                              </div>
                              <div class="box">
                                    <span class="label">E-mail</span>
                                    <input name="email" type="email">
                              </div>
                              <div class="box">
                                    <span class="label"><?=$language->get('phone')?>*</span>
                                    <input type="tel" name="phone" id="phone" required placeholder="+375 (   ) ___-__-__">
                                    <span class="label-right"><?=$language->get('on_which_they_made_the_order')?></span>
                              </div>
                              <textarea name="text" id="" cols="30" rows="10" placeholder="<?=$language->get('your_text')?>"></textarea>
                              <input id="submit" class="btn-red" style="display:none;" type="submit" value="<?=$language->get('send')?>">
                            <a onclick="send()" class="btn-red"><?=$language->get('send')?></a>
                        </form>
                  </div>
            </section>
            <section class="text-comment">
                  <div class="container">
                      <?php  foreach ($reviews as $review): ?>
                        <div class="box">
                              <p class="name"><?=$review['author']?></p>
                              <p class="text"><?=$review['text']?></p>
                              <p class="date"><?=date('d F Y',strtotime($review['date_added']))?></p>
                        </div>
                      <?php endforeach; ?>
                  </div>
            </section>
      </main>

<script>
    function send(){
        $('#submit').click();
        $.ajax({
            type: 'post',
            url: 'index.php?route=information/review',
            dataType: 'html',
            data:$("#send-normal").serialize(),
            success: function (html) {
                console.log(html);
                eval(html);
            }});


    }
</script>
<?php endif; ?>

<?php echo $footer; ?>