<?php if($module == 0): ?>
    <section class="slider-main">
        <div class="container">
            <?php foreach ($banners as $banner) { ?>
                <img src="<?php echo $banner['image']; ?>" alt="">
            <?php } ?>
        </div>
    </section>
<?php endif; ?>
<?php  if($module == 1): ?>
    <?php $i = 0; foreach ($banners as $banner) { ?>
        <section class="category-item <?= ($i % 2) ? 'reverse' : '' ?> blue-block">
            <div class="container">
                <div class="img">
                    <img src="<?= $banner['image']?>"<?= $banner['image']?> alt="">
                </div>
                <div class="content">
                    <div class="title">
                        <h2><?= $banner['title']?></h2>
                        <div class="line"></div>
                    </div>
                    <p><?= $banner['text']?></p>
                    <div class="read-more">
                        <div class="line"></div>
                        <a href="<?= $banner['link']?>" class="btn-red">Выбрать</a>
                    </div>
                </div>
            </div>
        </section>
    <?php $i++; } ?>
<?php endif; ?>

