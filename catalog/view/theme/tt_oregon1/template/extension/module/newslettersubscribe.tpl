<section class="subscribe blue-block">
    <div class="container">
        <h2><?=$language->get('Sign_up_for_the_newsletter')?></h2>
        <h3><?=$language->get('and_get_news_about_promotions_and_new_items')?></h3>
        <form name="subscribe" id="subscribe-normal">
            <p>
                <span><input  class="input" type="email" value="" name="subscribe_email" id="subscribe_email-normal" placeholder="e-mail"></span>
                <input type="hidden" value="" name="subscribe_name" id="subscribe_name-normal" />
                <a class="btn-red" onclick="email_subscribe()"><?=$language->get('send')?></a>
            </p>
        </form>
    </div>
</section>

<script type="text/javascript">
function email_subscribe(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=extension/module/newslettersubscribe/subscribe',
			dataType: 'html',
            data:$("#subscribe-normal").serialize(),
			success: function (html) {
				eval(html);
			}});


}
function email_unsubscribe(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=extension/module/newslettersubscribe/unsubscribe',
			dataType: 'html',
            data:$("#subscribe-normal").serialize(),
			success: function (html) {
				eval(html);
			}});
	$('html, body').delay( 1500 ).animate({ scrollTop: 0 }, 'slow');

}
</script>
<script type="text/javascript">
    $(document).ready(function() {
		$('#subscribe_email-normal').keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
                email_subscribe();
            }
			var name= $(this).val();
		  	$('#subscribe_name-normal').val(name);
        });
		$('#subscribe_email-normal').change(function() {
		 var name= $(this).val();
		  		$('#subscribe_name-normal').val(name);
		});

    });
</script>

