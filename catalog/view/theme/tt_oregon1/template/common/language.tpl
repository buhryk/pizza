<?php if (count($languages) >= 1) { ?>
    <div class="languages">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language">
            <?php foreach ($languages as $language) { ?>
                <?php if ($language['code'] == $code) { ?>
                    <a href="##" class="active"><?php echo $language['name']; ?></a>
                <?php } else { ?>
                    <a href="#" data-lanquage="<?= $language['code'] ?>" ><?php echo $language['name']; ?></a>
                <?php }  ?>
            <?php } ?>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        </form>
    </div>
<?php } ?>
