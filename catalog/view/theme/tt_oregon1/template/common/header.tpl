<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contrabanda</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="catalog/view/theme/tt_oregon1/style/style.css">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
    <script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script src="catalog/view/theme/tt_oregon1/js/slick.js"></script>
    <script src="catalog/view/theme/tt_oregon1/js/mask.js"></script>
    <script src="catalog/view/theme/tt_oregon1/js/script.js"></script>
<!--    --><?php //foreach ($styles as $style) { ?>
<!--<link href="--><?php //echo $style['href']; ?><!--" type="text/css" rel="--><?php //echo $style['rel']; ?><!--" media="--><?php //echo $style['media']; ?><!--" />-->
<?php //} ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">

<header class="header">
    <div class="top-line">
        <div class="container">
            <div class="phone-btn">
                <?php
                $telephones = explode(',', $telephone);
                ?>
                <div class="box">
                    <img src="catalog/view/theme/tt_oregon1/img/phone.png" alt="">
                    <a href="tel:<?= str_replace(' ', '', $telephones[0]) ?>"><?= $telephones[0] ?></a>
                </div>
                <div class="box">
                    <img src="catalog/view/theme/tt_oregon1/img/phone.png" alt="">
                    <a href="tel:<?= str_replace(' ', '', $telephones[1]) ?>"><?= $telephones[1] ?></a>
                </div>
            </div>
            <a href="##" class="call-back">
                <?=$languages->get('Request_a_call')?>
            </a>
            <div class="top-line-menu">
                <ul>
                    <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="mid-line">
        <div class="container">
            <?php echo $language; ?>
            <a href="/"><h1><?=$name?></h1></a>
            <h2><?=$comment?></h2>
        </div>
    </div>
    <div class="bottom-line">
        <div class="container">
            <div class="categories-list">
                <ul>
                    <?php foreach ($categories as $category): ?>
                    <li><a href="<?=$category['href']?>"><?=$category['name']?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <a href="<?=$cart_url?>" class="cart-block">
                <img src="catalog/view/theme/tt_oregon1/img/cart.png" alt="">
                <?=$languages->get('cart')?>  <span class="cart_count"> <?= $cart_count ?> </span>
            </a>
        </div>
    </div>
    <div class="mob-menu-btn">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <div class="mob-menu">
        <div class="container">
            <div class="top-menu-cont">
                <ul>
                    <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="main-menu-cont">
                <ul>
                    <?php foreach ($categories as $category): ?>
                        <li><a href="<?=$category['href']?>"><?=$category['name']?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="zvonok">
                <a href="tel:<?= str_replace(' ', '', $telephones[0]) ?>"><?= $telephones[0] ?></a>
                <a href="tel:<?= str_replace(' ', '', $telephones[1]) ?>"><?= $telephones[1] ?></a>
                <span></span>
                <a href="##"><?=$languages->get('Request_a_call')?></a>
            </div>
            <div class="languages-mob">
                <?= $language ?>
            </div>
        </div>
    </div>
</header>

<script type="text/javascript">
$(document).ready(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 179) {
		$('.header-menu').addClass("fix-nav");
		} else {
		$('.header-menu').removeClass("fix-nav");
		}
	});
	$(function dropDown()
	{
		elementClick = '#form-language .btn-language,#form-currency .btn-currency,#top-links .link-account,#cart .btn-cart';
		elementSlide =  '.dropdown-menu';
		activeClass = 'active';

		$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
		subUl.slideDown();
		$(this).addClass(activeClass);
		}
		else
		{
		subUl.slideUp();
		$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
		});

		$(elementSlide).on('click', function(e){
		e.stopPropagation();
		});

		$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
		});
	});
});
</script>