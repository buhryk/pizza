<?php echo $header; ?>
    <main class="page-order">
        <section class="title-block">
            <div class="conainer">
                <h1 class="section-title"><?= $languages->get('button_checkout') ?></h1>
            </div>
        </section>
        <section class="order-table">
            <div class="container">
                <div class="table-headers">
                    <p class="prod"><?= $languages->get('name') ?></p>
                    <p class="size"><?= $languages->get('size') ?></p>
                    <p class="num"><?= $languages->get('quantity') ?></p>
                    <p class="summ"><?= $languages->get('sum') ?></p>
                    <p class="dell"><?= $languages->get('remove') ?></p>
                </div>
                <div class="table-body">
                    <?php
                    //                    pr($products);
                    foreach ($products

                    as $product) { ?>
                    <?php if (!$product['product_dob']): ?>
                    <div class="product-row">
                        <div class="prod" data-product_id="<?php echo $product['product_id']; ?>"
                             data-key="<?php echo $product['key']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="">
                            <div class="text">
                                <h3><?php echo $product['name']; ?></h3>

                                <?php if (isset($product['attributes'][0]['attribute'])): ?>
                                    <?php foreach ($product['attributes'][0]['attribute'] as $attribute): ?>


                                        <?php if ($attribute['attribute_id'] == 12) {
                                            ; ?>
                                            <p>
                                                <?= $attribute['text'] ?>
                                            </p>

                                        <?php } ?>

                                    <?php endforeach; ?>
                                <?php endif; ?>

                                <?php $total = 0;
                                if (isset($dobavky[$product['product_id']])): ?>
                                    <br><h3>Добавки:</h3>
                                    <p>
                                        <?php


                                        foreach ($dobavky[$product['product_id']] as $item) {
                                            $total += $item['total'] ?>

                                            <?= $item['name'] ?> - <?= $item['quantity'] ?>,
                                        <?php } ?>
                                    </p>
                                <?php endif; ?>

                            </div>
                        </div>
                        <div class="size">
                            <?php
                            //                            pr($product['option']);
                            foreach ($product['consistOptions'] as $consistOption) {
                                if ($consistOption['option_id'] == 13) {
                                    foreach ($consistOption['product_option_value'] as $item) {
                                        ; ?>
                                        <div class="box">
                                            <input class="<?= $product['option'][0]['value'] == $item['name'] ? 'active' : '' ?>" type="radio" id="<?= $item['product_option_value_id'] ?>"
                                                   name="option[<?php echo $consistOption['product_option_id']; ?>]"
                                                   data-price="<?= $item['price'] ?>"
                                                   data-option="<?= $item['product_option_value_id'] ?>"
                                                   value="<?php echo $item['product_option_value_id']; ?>"
                                                <?= $product['option'][0]['value'] == $item['name'] ? 'checked' : '' ?>>
                                            <label for="<?= $item['product_option_value_id'] ?>"><?= $item['name'] ?></label>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="num">
                            <div class="number" data-price="<?php echo $product['price']; ?>">
                            <div class="minus" onclick="update(this, 'updatem')">-</div>
                            <input class="input" type="number"
                                   name="quantity"
                                   id="input-quantity"
                                   value="<?php echo $product['quantity']; ?>" min="1">
                            <input name="product_id" type="hidden" value="<?= $product['product_id'] ?>">
                            <div class="plus" onclick="update(this, 'updatep')">+</div>
                        </div>
                    </div>
                    <div class="summ" data-price="<?php echo $product['price'] ?>" data-total="<?php echo $total; ?>">
                        <p><?php echo $product['total'] + $total; ?> Br</p>
                    </div>
                    <div class="dell" data-test="test" onclick="update(this, 'remove')">
                        <div class="dell-btn">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php } ?>
            </div>
            </div>
        </section>
        <section class="total-price">
            <div class="container">
                <p class="title"><?= $languages->get('Free_information') ?></p>
                <div class="numbers">
                    <!--                    --><?php //if($total_dob): ?>
                    <!--                        <div class="box">-->
                    <!--                            <p class="n">Добавки</p>-->
                    <!--                            <p class="p">--><? //= $total_dob ?><!-- Br</p>-->
                    <!--                        </div>-->
                    <!--                    --><?php //endif; ?>
                    <div class="box">
                        <p class="n"><?= $languages->get('delivery') ?></p>
                        <p class="p">0 Br</p>
                    </div>
                    <div class="box">
                        <p class="n"><?= $languages->get('Order_price') ?></p>
                        <p class="p" id="cart-total"><?= $totals[0]['text'] ?> Br</p>
                    </div>
                </div>
            </div>
        </section>
        <div class="title-del">
            <div class="container">
                <h2 class="section-title"><?= $languages->get('Shipping_and_Payment_Information') ?></h2>
            </div>
        </div>
        <section class="delivery">
            <div class="container">
                <form action="">
                    <div class="left">
                        <div class="input-box">
                            <p><?= $languages->get('Full_name') ?></p>
                            <input type="text" name="firstname"
                                   value="<?php if (isset($address['firstname'])) echo $address['firstname']; elseif (isset($firstname)) echo $firstname; ?>"
                                   class="input">
                        </div>
                        <div class="input-box">
                            <p><?= $languages->get('phone') ?></p>
                            <input type="tel" name="telephone"
                                   value="<?php echo (isset($telephone_user)) ? $telephone_user : $telephone; ?>"
                                   class="input">
                        </div>
                        <div class="input-box">
                            <p>E-mail</p>
                            <input type="email" name="email"
                                   value="<?php echo (isset($email_user)) ? $email_user : $email; ?>" class="input">
                        </div>
                        <div class="input-box">
                            <p><?= $languages->get('Delivery_address') ?></p>
                            <input type="text" name="address_1" class="input"
                                   placeholder="<?= $languages->get('Street_house') ?>">
                            <div class="inputs">
                                <input class="input" name="entrance" type="text"
                                       placeholder="<?= $languages->get('entrance') ?>">
                                <input class="input" name="floor" type="text"
                                       placeholder="<?= $languages->get('Floor') ?>">
                                <input class="input" name="flat" type="text"
                                       placeholder="<?= $languages->get('Apartment') ?>">
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <h2><?= $languages->get('Select_a_Payment_Method') ?></h2>
                        <div class="pay-var">
                            <?php foreach ($payment_methods as $method): ?>
                                <input type="radio" name="payment_method" value="<?= $method['code'] ?>"
                                       id="<?= $method['code'] ?>" checked>
                                <label for="<?= $method['code'] ?>">
                                    <img src="/catalog/view/theme/tt_oregon1/img/pay<?= $method['code'] == 'cheque' ? '1' : '2' ?>.png"
                                         alt="">
                                    <p><?= $method['title'] ?></p>
                                </label>
                            <?php endforeach; ?>
                        </div>
                        <h2><?= $languages->get('Comment_to_the_order') ?></h2>
                        <textarea name="comment" id="" cols="30" rows="10"></textarea>
                        <div class="last-line">
                            <p class="summa"><?= $languages->get('Order_price') ?>: <span
                                        id="cart-total2"><?= $totals[0]['text'] ?> Br</span></p>
                            <!--                            <button class="btn-red" id="button-go">Оформить заказ</button>-->
                        </div>
                        <div class="col-sm-12 oct-fastorder-payment text-right">
                            <!--                            --><?php //if (isset($payment) && $payment) {?>
                            <!--                                <p>--><?php //$payment; ?><!--</p>-->
                            <!--                            --><?php //} else { ?>
                            <label class="errors"></label>
                            <input type="button" class="btn-red" id="button-go"
                                   value="<?= $languages->get('Checkout') ?>"/>
                            <!--                            --><?php //} ?>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>


    <script>
        $('body').delegate('#button-go', 'click', function () {
            var data = $('input[type=\'text\'], input[type=\'email\'], input[type=\'tel\'], textarea, input[name=\'payment_method\']:checked');

            // if (!error) {
            $.ajax({
                url: 'index.php?route=checkout/oct_fastorder/validate_form',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function (json) {
                    $('.text-danger').remove();

                    if (json['error']) {
                        for (i in json['error']) {
                            console.log(i);
                            $('input[name=' + i + ']').after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    } else {

                        $.ajax({
                            url: 'index.php?route=checkout/oct_fastorder/confirm',
                            type: 'post',
                            data: data,

                            success: function (url) {
                                console.log(url);

                                location = url;
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
            // }
        });

        $('.size input[type=\'radio\']').on('click', function () {
            var target = this;
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: $(target).closest('.product-row').find('input[name=\'product_id\'], input[name=\'quantity\'],  .size input[type=\'radio\']:checked'),
                dataType: 'json',

                success: function (json) {

                    $(target).closest('.product-row').find('.dell').click();
                    location.reload();

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        function update(target, status) {

            var input_val = $(target).parent().parent().parent().find('input[name=quantity]').val(),
                quantity = parseInt(input_val),
                product_id = $(target).parent().parent().parent().find('.prod').data('product_id'),
                product_key = $(target).parent().parent().parent().find('.prod').data('key'),
                urls = null;

            if (quantity <= 0) {
                // quantity = $(target).parent().parent().children('input[name=quantity]').val(1);
                return;
            }

            if (status == 'updatep') {
                quantity += 1;
                urls = 'index.php?route=checkout/oct_fastorder/cart&update=' + product_key + '&quantity=' + quantity;
            }
            if (status == 'updatem') {
                quantity -= 1;
                urls = 'index.php?route=checkout/oct_fastorder/cart&update=' + product_key + '&quantity=' + quantity;
            } else if (status == 'add') {
                urls = 'index.php?route=checkout/oct_fastorder/cart&add=' + target + '&quantity=1';
            }
            console.log(product_key + ' q');

            if (status == 'remove') {

                product_key = $(target).parent().find('.prod').data('key'),
                    urls = 'index.php?route=checkout/oct_fastorder/cart&remove=' + product_key;
                $.ajax({
                    url: urls,
                    type: 'get',
                    dataType: 'html',
                    beforeSend: function (e) {
                        $(target).parent().after().remove();
                    },
                    success: function (data) {
                        $.ajax({
                            url: 'index.php?route=checkout/oct_fastorder/status_cart',
                            type: 'get',
                            dataType: 'json',
                            success: function (json) {
                                if (json['total']) {
                                    $('#cart-total').html(json['total'] + ' Br');
                                    $('#cart-total2').html(json['total'] + ' Br');
                                    $('.cart_count').html(json['count']);
                                }else {
                                    location.reload();
                                }
                            }
                        });
                    }

                });
            } else {
                $.ajax({
                    url: urls,
                    type: 'get',
                    dataType: 'html',
                    beforeSend: function (e) {
                        // $(target).parent().html('');
                    },
                    success: function (data) {
                        // console.log(data);
                        $.ajax({
                            url: 'index.php?route=checkout/oct_fastorder/status_cart',
                            type: 'get',
                            dataType: 'json',
                            success: function (json) {
                                if (json['total']) {
                                    $('#cart-total').html(json['total'] + ' Br');
                                    $('#cart-total2').html(json['total'] + ' Br');
                                    $('.cart_count').html(json['count']);
                                }
                            }
                        });
                        // $.ajax({
                        //     type: 'get',
                        //     success: function (data) {
                        //         // console.log($(data).find('.table-body').html());
                        //         $('.table-body').load($(data).find('.checkout-oct_fastorder').html());
                        //         // $('.checkout-oct_fastorder').html($(data).find('.checkout-oct_fastorder').html())
                        //     },
                        //     // timeout: 3000,async: false
                        //
                        // });
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        }

    </script>

<?php echo $footer; ?>