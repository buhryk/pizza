<?php echo $header; ?>
<main class="page-archive">
    <section class="title-block">
        <h2 class="section-title"><?php echo $heading_title; ?></h2>
    </section>
    <section class="products-list sous">
        <div class="container">
            <div class="img">
                <img src="<?= $thumb ?>" alt="">
            </div>
            <div class="sous-list">
                <ul>
                    <?php foreach ($products as $product): ?>
                        <li>
                            <p class="name"><?= $product['name'] ?></p>
                            <p class="weight">10 г / <?= $product['price'] ?> Br</p>
                            <div class="number">
                                <div class="minus">-</div>
                                <form>
                                    <input class="input" name="quantity" type="number" value="1" min="1">
                                    <input  name="product_id" type="hidden"  value="<?= $product['product_id'] ?>" >
                                </form>
                                <div class="plus">+</div>
                            </div>
                            <div data-src="##" class="btn-red"><img src="/catalog/view/theme/tt_oregon1/img/cart.png" alt=""> Заказать</div>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </section>
</main>

<script>
    $(document).ready(function() {


        $('.sous-list .btn-red').on('click', function() {
            console.log('qw');
            ajaxAddToCart($(this).parent().find('form'));

        });



        function ajaxAddToCart(data) {
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: data.serialize(),
                dataType: 'json',
                beforeSend: function () {
                    // $('#button-cart').button('loading');
                },
                complete: function () {
                    // $('#button-cart').button('reset');
                },
                success: function(json) {
                    $('.alert, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['error']) {
                        if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                var element = $('#input-option' + i.replace('_', '-'));

                                if (element.parent().hasClass('input-group')) {
                                    element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                } else {
                                    element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                }
                            }
                        }

                        if (json['error']['recurring']) {
                            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    }

                    if (json['success']) {

                        $('.cart_count').html(json['total']);

                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }


            });
        }
    });
</script>

