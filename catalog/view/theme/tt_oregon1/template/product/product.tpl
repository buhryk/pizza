<?php echo $header; ?>
<main class="page-single">
    <section class="title-mob">
        <div class="container">
            <h2 class="section-title"><?php echo $heading_title; ?></h2>
        </div>
    </section>
    <section class="product-card">
        <div class="container">
            <div class="left-coll">
                <img class="main-img" style="margin-bottom: 20px;" src="<?php echo $thumb; ?>" alt="">

                <?php  foreach ($options as $option): ?>
                    <?php  if ($option['option_id'] == 13) { ;?>
                        <div class="size">
                            <div class="row">
                                <div><?=$languages->get('portion')?></div>
                                <div><?=$languages->get('weight')?></div>
                                <div><?=$languages->get('size')?></div>
                            </div>
                            <?php $i = 1; foreach ($option['product_option_value'] as $item) { ?>
                                <div class="row act <?= $i == 1 ? 'active' : ''?>">
                                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]"  value="<?php echo $item['product_option_value_id']; ?>">

                                    <div class="option" data-option="<?= $item['product_option_value_id'] ?>" data-priceOption="<?= $item['price'] ?>">
                                        <?php $j = $i; while ($j): ?>
                                            <img src="catalog/view/theme/tt_oregon1/img/person.png"  alt="">
                                        <?php $j--; endwhile; ?>
                                    </div>
                                    <div><?=(int)$item['weight']?> г</div>
                                    <div><?=$item['name']?></div>
                                </div>
                            <?php $i++; } ?>

                        </div>
                    <?php } ?>
                <?php endforeach; ?>

                <div class="number" data-price="<?= $price ?>">
                    <div class="minus">-</div>
                    <form id="product-data">
                        <input class="input" name="quantity" id="input-quantity" type="number" value="1" min="1">
                        <input  name="product_id" type="hidden"  value="<?= $product_id ?>" >
                    </form>
                    <div class="plus">+</div>
                </div>
                <div class="bottom">
                    <span class="price"><?= $price ?> Br</span>
                    <div data-src="##" id="button-cart" class="btn-red"><img src="catalog/view/theme/tt_oregon1/img/cart.png" alt=""> <?=$languages->get('order')?></div>
                </div>
            </div>
            <div class="right-coll">
                <div class="title-block">
                    <h1 class="section-title"><?php echo $heading_title; ?></h1>
                    <img src="catalog/view/theme/tt_oregon1/img/imgsss.png" alt="">
                </div>
                <?php if (isset($attribute_groups[0]['attribute'])): ?>
                    <?php  foreach ($attribute_groups[0]['attribute'] as $attribute): ?>
                        <?php if ($attribute['attribute_id'] == 5) {  ;?>
                            <div class="weight-block" >
                                <ul>
                                    <h3><?=$attribute['name']?>: <span><?=$attribute['text']?></span></h3>
                                </ul>
                            </div>
                        <?php } ?>
                            <div class="sostav">
                        <?php if ($attribute['attribute_id'] == 12) {  ;?>
                                <h3><?=$attribute['name']?>:</h3>
                                <ul>
                                    <?php
                                    $elements = explode(',', $attribute['text']);
                                    foreach ($elements as $item) { ?>
                                        <li><?=$item?></li>
                                    <?php } ?>
                                </ul>
                            <?php } if ($attribute['attribute_id'] == 13){ ?>
                                <h3><?=$attribute['name']?>:</h3>
                                <p><?=$attribute['text']?></p>
                        <?php } ?>
                            </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if (isset($categories)): ?>
                    <div class="dobavki">
                        <h3><?=$languages->get('Supplements')?>:</h3>
                        <div class="dobavki-container">
                            <div class="d-header">
                                 <?php $i = 0; foreach ($categories as $category): ?>
                                    <p class="<?= $i == 0 ? 'active' : ''?>"><?= $category['name'] ?></p>
                                 <?php $i++; endforeach; ?>
                            </div>
                            <div class="d-body">
                                <?php  $i = 0; foreach ($categories as $category): ?>
                                    <div class="tab <?= $i == 0 ? 'active' : ''?>">
                                        <?php foreach ($category['products'] as $product): ?>
                                            <?php
                                                if (isset($cart_products[$product['product_id'] + $product_id])) {
                                                    $cart_product =  $cart_products[$product['product_id'] + $product_id];
                                                }else{
                                                    $cart_product =  [];
                                                }
                                            ?>
                                            <div class="box">
                                            <p class="title"><?= $product['name'] ?></p>
                                            <div class="price">50 г / <?= $product['price'] ?> Br</div>
                                            <div class="number">
                                                <div class="minus d" data-cart="<?= $cart_product['cart_id'] ?>" >-</div>
                                                <input class="input" type="number" value="<?= $cart_product['quantity'] ?? 0 ?>" min="0">
                                                <div class="plus d" onclick="cart.add('<?= $product['product_id'] ?>', '', '<?= $product_id ?>')">+</div>
                                            </div>
                                        </div>
                                        <?php $i++; endforeach; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="text">
                    <textarea name="comment"  id="" cols="30" rows="10" placeholder="<?=$languages->get('Comment_to_the_recipe_if_necessary')?>"></textarea>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript"><!--


    var option_price = parseInt($('.size .active').find('.option').data('priceoption'));
    option_price = option_price ? option_price : 0;
    var product_price = parseInt($('.number').data('price'));
    var general_price = option_price + product_price;
    $('.bottom .price').text(general_price + ' Br');


    $('.d-body .minus').on('click', function() {
        cart_id = $(this).data('cart');
        quantity = parseInt($(this).next().val() - 1);
        console.log(quantity + ' qqqqq');
        cart.update(cart_id, quantity);
    });

    $('.size .row').on('click', function() {
        var option_price = parseInt($(this).find('.option').data('priceoption'));
        option_price = option_price ? option_price : 0;
        var product_price = parseInt($('.number').data('price'));
        var general_price = option_price + product_price;
        var quantity = parseInt($('.number input[name=\'quantity\']').val());
        var price = general_price * quantity ;

        $('.bottom .price').text(price + ' Br');
    });


    $('#button-cart').on('click', function() {

        var options_id = '';
        $('.box input[type=\'number\']').each( function( index, value ) {

            if($(value).val() > 0){
                options_id += '#' + $(value).prev().attr('id') + ', '
            }
        });
        // options_id = options_id.substr(0, options_id.length - 2)
        console.log(options_id);

         $.ajax({
             url: 'index.php?route=checkout/cart/add',
             type: 'post',
             data: $('input[name=\'product_id\'], #input-quantity, ' + options_id + ' .size .active input[type=\'radio\'], textarea[name=\'comment\']'),
             dataType: 'json',
             beforeSend: function() {
 //			$('#button-cart').button('loading');
             },
             complete: function() {
 //			$('#button-cart').button('reset');
             },
             success: function(json) {
                 $('.alert, .text-danger').remove();
                 $('.form-group').removeClass('has-error');

                 if (json['error']) {
                     if (json['error']['option']) {
                         for (i in json['error']['option']) {
                             var element = $('#input-option' + i.replace('_', '-'));

                             if (element.parent().hasClass('input-group')) {
                                 element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                             } else {
                                 element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                             }
                         }
                     }

                     if (json['error']['recurring']) {
                         $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                     }

                     // Highlight any found errors
                     $('.text-danger').parent().addClass('has-error');
                 }

                 if (json['success']) {
                     // $('body').before('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                     $('.cart_count').html(json['total']);

                     // $('html, body').animate({ scrollTop: 0 }, 'slow');

                     $('#cart > .top-cart-contain ul').load('index.php?route=common/cart/info ul li');
                 }
             },
             error: function(xhr, ajaxOptions, thrownError) {
                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
             }
         });
    });

//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});
var minimum = <?php echo $minimum; ?>;
  $("#input-quantity").change(function(){
    if ($(this).val() < minimum) {
      alert("Minimum Quantity: "+minimum);
      $("#input-quantity").val(minimum);
    }
  });
  // increase number of product
  function minus(minimum){
      var currentval = parseInt($("#input-quantity").val());
     //  $("#input-quantity").val(currentval-1);
     //  if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
     //      alert("Minimum Quantity: "+minimum);
     //      $("#input-quantity").val(minimum);
     // }
  };
  // decrease of product
  function plus(){
      var currentval = parseInt($("#input-quantity").val());
     // $("#input-quantity").val(currentval+1);
  };
  $('.minus').click(function(){
    minus(minimum);
  });
  $('.plus').click(function(){
    plus();
  });
  // zoom
	$(".thumbnails img").elevateZoom({
		zoomType : "window",
		cursor: "crosshair",
		gallery:'gallery_01',
		galleryActiveClass: "active",
		imageCrossfade: true,
		responsive: true,
		zoomWindowOffetx: 0,
		zoomWindowOffety: 0,
	});
	// slider
	$(".image-additional").owlCarousel({
		navigation: true,
		pagination: false,
		slideSpeed : 500,
		goToFirstSpeed : 1500,
		addClassActive: true,
		items : 4,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [1024,3],
		itemsTablet: [640,2],
		itemsMobile : [480,2],
		afterInit: function(){
			$('#gallery_01 .owl-wrapper .owl-item:first-child').addClass('active');
		},
		beforeInit: function(){
			$(".image-additional .thumbnail").show();
		}
	});
	$('#gallery_01 .owl-item .thumbnail').each(function(){
		$(this).click(function(){
			$('#gallery_01 .owl-item').removeClass('active2');
			$(this).parent().addClass('active2');
		});
	});
	// related products
	$(".related-product").owlCarousel({
		navigation: true,
		pagination: false,
		slideSpeed : 500,
		goToFirstSpeed : 1500,
		items : 5,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [767,3],
		itemsTablet: [640,2],
		itemsMobile : [480,1],
		addClassActive: true,
		afterAction: function(el){
		   this.$owlItems.removeClass('before-active')
		   this.$owlItems.removeClass('last-active')
		   this.$owlItems .eq(this.currentItem).addClass('before-active')
		   this.$owlItems .eq(this.currentItem + (this.owl.visibleItems.length - 1)).addClass('last-active')
		}
	});
//--></script>
<?php echo $footer; ?>
