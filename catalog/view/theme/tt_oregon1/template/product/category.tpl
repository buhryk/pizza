<?php echo $header; ?>
<main class="page-archive">
    <section class="title-block">
        <h2 class="section-title"><?php echo $heading_title; ?></h2>
    </section>
    <section class="filter">
        <div class="container">
            <?php echo $content_top; ?>

        </div>
    </section>
    <section class="products-list">
        <div class="container">
            <ul>
                <?php
//pr($products);
                foreach ($products as $product) {  ?>
                <li style="background-image: url(<?= $product['thumb'] ?>)">
                    <a href="<?= $product['href'] ?>">
                    <div class="prod-card" data-href="##">
                        <p class="title"><?= $product['name'] ?></p>
                        <div class="bottom">
                            <span class="price"><?= $product['price'] ?> Br</span>
                            <div data-src="##" class="btn-red" id="order"><img src="/catalog/view/theme/tt_oregon1/img/cart.png" alt=""> <?=$languages->get('order')?></div>
                        </div>

                            <div class="opacity">
                                <?php if (isset($product['attributes'][0]['attribute'])): ?>
                                    <?php  foreach ($product['attributes'][0]['attribute'] as $attribute): ?>
                                        <?php if ($attribute['attribute_id'] == 5) {  ;?>
                                            <p class="weight"><?=$attribute['text']?></p>
                                            <div class="line"></div>
                                        <?php } ?>

                                        <?php if ($attribute['attribute_id'] == 12) {  ;?>
                                            <p class="description">
                                                <?= $attribute['text']?>
                                            </p>
                                            <div class="line"></div>
                                        <?php } ?>

                                    <?php endforeach; ?>
                                <?php endif; ?>

                                <form>
                                <div class="radio">
                                    <?php
//                                    pr($product['consistOptions']);
                                    foreach  ($product['consistOptions'] as $consistOption){
                                        if ($consistOption['option_id'] == 13){
                                            foreach ($consistOption['product_option_value'] as $item){ ?>
                                                <div class="box">
                                                    <input type="radio" id="<?=$item['product_option_value_id']?>"
                                                           name="option[<?php echo $consistOption['product_option_id']; ?>]"
                                                           value="<?php echo $item['product_option_value_id']; ?>"
                                                           data-price="<?=$item['price']?>"
                                                           data-option_value_id="<?=$item['option_value_id']?>"
                                                           data-product_option_value_id="<?=$consistOption['product_option_id']?>"
                                                           checked>
                                                    <label for="<?=$item['product_option_value_id']?>"><?=$item['name']?></label>
                                                </div>
                                    <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="number" data-price="<?= $product['price'] ?>">
                                    <div class="minus">-</div>

                                        <input class="input" name="quantity" id="input-quantity" type="number" value="1" min="1">
                                        <input  name="product_id" type="hidden"  value="<?= $product['product_id'] ?>" >

                                    <div class="plus">+</div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </section>
</main>
<script>
    $(document).ready(function() {

        $('.box input:checked').each( function( index, value ) {
            var option_price = parseInt($( this ).data('price'));
            var product_price = parseInt($( this ).parent().parent().parent().find('.number').data('price'));
            var general_price = option_price + product_price;

            $( this ).closest('.prod-card').find('.price').text(general_price + ' Br');
        });

        $('input[type=\'radio\']').on('click', function() {
            var option_price = parseInt($( this ).data('price'));
            var product_price = parseInt($( this ).parent().parent().parent().find('.number').data('price'));
            var general_price = option_price + product_price;


            var quantity = parseInt( $( this ).closest('.prod-card').find('input[name=\'quantity\']').val());
            var price = general_price * quantity ;

            $( this ).closest('.prod-card').find('.price').text(price + ' Br');
        });


        $('.number').on('click', function(e) {
            e.preventDefault();
        });

        $('.bottom').on('click', function(e) {
            e.preventDefault();


            ajaxAddToCart($(this).parent().find('form'));

        });


        function ajaxAddToCart(data) {
            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: data.serialize(),
                dataType: 'json',

                success: function(json) {
                    console.log(json);
                    if (json['success']) {

                        $('.cart_count').html(json['total']);

                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
</script>
<?php echo $footer; ?>
