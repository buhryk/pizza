<?php
// Heading
$_['heading_title']				= 'Thank you for shopping with %s .... ';

// Text
$_['text_title']				= 'Credit Card / Debit Card (PayPoint)';
$_['text_response']				= 'Response from PayPoint:';
$_['text_success']				= '... your payment was successfully received.';