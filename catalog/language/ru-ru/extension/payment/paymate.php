<?php
// Text
$_['text_title']				= 'Credit Card / Debit Card (Paymate)';
$_['text_unable']				= 'Unable to locate or update your order status';
$_['text_declined']				= 'Payment was declined by Paymate';
$_['text_failed']				= 'Paymate Transaction Failed';
$_['text_basket']				= 'Basket';
$_['text_checkout']				= 'Checkout';
$_['text_success']				= 'Success';