<?php
// Heading
$_['heading_title'] = 'Мои закладки';

// Text
$_['text_account']  = 'Личный Кабинет';
$_['text_instock']  = 'В наличии';

$_['text_remove']   = 'Список закладок успешно изменен!';
$_['text_empty']    = 'Ваши закладки пусты';

// Column
$_['column_image']  = 'Изображение';
$_['column_name']   = 'Название товара';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'Наличие';
$_['column_price']  = 'Цена за единицу товара';
$_['column_action'] = 'Действие';

