<?php
// Heading 
$_['heading_title'] 	 = 'Присоединяйтесь к нашей компании';

$_['newletter_lable'] 	 = 'Be the First to Know. Sign up for newsletter today';
$_['newletter_des'] 	 = 'Подпишитесь на наши информационные бюллетени и не забудьте ознакомиться с новыми коллекциями, последними книгами и эксклюзивными предложениями.';

//Fields
$_['entry_email'] 		 = 'Email';
$_['entry_name'] 		 = 'Name';

//Buttons
$_['entry_button'] 		 = 'Подписывайся';
$_['entry_unbutton'] 	 = 'Unsubscribe';

//text
$_['text_subscribe'] 	 = 'Подписаться здесь';

$_['mail_subject']   	 = 'NewsLetter Subscribe';

//Messages
$_['error_invalid'] 	 = 'Error : Please enter valid email address.';
$_['subscribe']	    	 = 'Success : Subscribed Successfully.';
$_['unsubscribe'] 	     = 'Success : Unsubscribed Successfully.';
$_['alreadyexist'] 	     = 'Error : Email already exist.';
$_['notexist'] 	    	 = 'Error : Email doesn\'t exist.';
$_['entry_show_again']   =  "Не показывать это всплывающее окно снова";